﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractiveMenuController : PageUI
{
    #region Struct

    [System.Serializable]
    public struct Profession
    {
        public Button button;
        public int sceneIndex;
    }

    #endregion

    #region Unity Edit

    [Header("UI")]
    [SerializeField] private List<Profession> _professions;
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _geoButton;


    [Header("Pages")]

    [SerializeField] private PageUI _backPage;
    [SerializeField] private PageUI _multiplayerRoomPage;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        /*Debug.Log(_professions.Count);
        for (int i = 0; i < _professions.Count; i++)
        {
            _professions[i].button.onClick.AddListener(() =>
            {
                Debug.Log(_professions[i].sceneIndex);
                _launcher.StartGame(_professions[i].sceneIndex);
            });
        }*/
        _geoButton.onClick.AddListener(() =>
        {
            _multiplayerRoomPage.Show();
            Hide();
        });

        _backButton.onClick.AddListener(() =>
        {
            _backPage.Show();
            Hide();
        });
    }

    #endregion


}
