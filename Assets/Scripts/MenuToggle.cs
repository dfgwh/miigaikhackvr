﻿using UnityEngine;
using VRTK;

public class MenuToggle : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private VRTK_ControllerEvents _vrtkControllerEvents;
    [SerializeField] private GameObject _menuObgect;

    #endregion

    #region Private Fields

    private bool _menuState = false;

    #endregion

    #region Methods

    private void OnEnable()
    {
        //_vrtkControllerEvents.ButtonTwoPressed += _vrtkControllerEvents_ButtonTwoPressed;
        _vrtkControllerEvents.ButtonTwoReleased += _vrtkControllerEvents_ButtonTwoReleased;
    }

    private void OnDisable()
    {
        //_vrtkControllerEvents.ButtonTwoPressed -= _vrtkControllerEvents_ButtonTwoPressed;
        _vrtkControllerEvents.ButtonTwoReleased -= _vrtkControllerEvents_ButtonTwoReleased;
    }

    private void _vrtkControllerEvents_ButtonTwoPressed(object sender, ControllerInteractionEventArgs e)
    {
        _menuState = !_menuState;
        _menuObgect.SetActive(_menuState);
    }

    /// <summary>
    /// включение/выключение
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void _vrtkControllerEvents_ButtonTwoReleased(object sender, ControllerInteractionEventArgs e)
    {
        _menuState = !_menuState;
        _menuObgect.SetActive(_menuState);
    }

    #endregion
}
