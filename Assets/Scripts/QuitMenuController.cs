﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitMenuController : PageUI
{
        #region Unity Editor

    [Header("UI")]
    [SerializeField] private Button _agreeButton;
    [SerializeField] private Button _notAgreeButton;

    [Header("Pages")]
    [SerializeField] private MainMenuController _mainMenuController;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _agreeButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });
        _notAgreeButton.onClick.AddListener(() =>
        {
            Hide();
            _mainMenuController.Show();
        });
    }

    #endregion
}
