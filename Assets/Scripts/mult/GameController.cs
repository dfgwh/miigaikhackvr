﻿using Photon.Pun;

using UnityEngine;

public class GameController : MonoBehaviourPun
{
    #region Unity Editor

    public Transform pos;
    [SerializeField] private string prefabName = "User";

    #endregion

    private void Awake()
    {
        var obj = PhotonNetwork.Instantiate(prefabName, pos.position + new Vector3(0,1,0), Quaternion.identity);
        obj.transform.SetParent(pos);
    }
}
