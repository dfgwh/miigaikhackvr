﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class NetworkController : MonoBehaviourPunCallbacks
{
    public Text txtStatus = null;
    public GameObject startBatton = null;
    public byte maxPlayers = 4;

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
        startBatton.SetActive(false);
        Status("Connecting to server");
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        PhotonNetwork.AutomaticallySyncScene = true;
        startBatton.SetActive(true);
        Status("Connected to " + PhotonNetwork.ServerAddress);
    }

    public void btnStart_Click()
    {
        string roomName = "Room1";
        Photon.Realtime.RoomOptions opts = new Photon.Realtime.RoomOptions();
        opts.IsOpen = true;
        opts.IsVisible = true;
        opts.MaxPlayers = maxPlayers;

        PhotonNetwork.JoinOrCreateRoom(roomName, opts, Photon.Realtime.TypedLobby.Default);
        startBatton.SetActive(false);
        Status("joining " + roomName);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        PhotonNetwork.LoadLevel(1);
        //SceneManager.LoadScene(1);
    }

    private void Status(string msg)
    {
        Debug.Log(msg);
        txtStatus.text = msg;
    }
}
