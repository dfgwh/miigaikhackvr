﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Launcher : MonoBehaviourPunCallbacks
{

    #region Unity Methods

    private void Start()
    {
        Debug.Log("Connecting tomaster ...");
        PhotonNetwork.ConnectUsingSettings();
    }

    #endregion

    #region Override Methods

    public override void OnConnectedToMaster()
    {
        //base.OnConnectedToMaster();
        Debug.Log("Connected to master");
        PhotonNetwork.JoinLobby();
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnJoinedLobby()
    {
        //base.OnJoinedLobby();
        Debug.Log("Join Lobby");
        CreateRoom();
    }

    public override void OnJoinedRoom()
    {
        //base.OnJoinedRoom();
        Debug.Log("Joined to room");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        //base.OnCreateRoomFailed(returnCode, message);
    }

    public override void OnLeftRoom()
    {
        //base.OnLeftRoom();
        Debug.Log("Left room");
    }

    #endregion

    #region Public Methods

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom("roomTest");
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void StartGame(int index)
    {
        CreateRoom();
        PhotonNetwork.LoadLevel(index);
        Debug.Log("index " + index);
    }

    #endregion
}
