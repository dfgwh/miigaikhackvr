﻿using UnityEngine;

public class RotateToUser : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private Transform _target;

    #endregion

    #region Unity Methods

    /// <summary>
    /// Активация поворота при появлении
    /// </summary>
    private void Update()
    {
        RotateOnTarget();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Поворот относительно цели
    /// </summary>
    private void RotateOnTarget()
    {
        transform.LookAt(_target);
        transform.Rotate(Vector3.right * transform.rotation.x
            + Vector3.up * (transform.rotation.y)
            + Vector3.forward
        );
    }

    #endregion
}
