﻿using UnityEngine;
using UnityEngine.UI;

public class AdminLoginMenuController : PageUI
{
    #region Constants

    private const string PASSWORD = "admin0qwerty123"; //для прототипа просто хардкодим

    #endregion

    #region Unity Editor

    [SerializeField] private InputField _passwordInputField;
    [SerializeField] private Button _passwordButton;

    [SerializeField] private Button _backButton;
    [SerializeField] private PageUI _backPage;
    [SerializeField] private PageUI _nextPage;



    #endregion

    #region Private Fields

    private bool isAdmin = true;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _passwordButton.onClick.AddListener(() =>
        {
            if (_passwordInputField.text == PASSWORD)
            {
                isAdmin = true;
                _nextPage.Show();
                Hide();
            }
        });

        _backButton.onClick.AddListener(() =>
        {
            _backPage.Show();
            Hide();
        });
    }


    #endregion

    #region Override Methods

    public override void Show()
    {
        base.Show();

        if (isAdmin)
        {
            _nextPage.Show();
            Hide();
        }
    }

    #endregion
}
