﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : PageUI
{

    #region Unity Editor

    [Header("UI")]
    [SerializeField] private Button _settingsButton;
    [SerializeField] private Button _quitButton;
    [SerializeField] private Button _adminButton;
    [SerializeField] private Button _interactiveButton;

    [Header("Pages")]
    [SerializeField] private PageUI _settingsMenuController;
    [SerializeField] private PageUI _quitMenuController;
    [SerializeField] private PageUI _adminPage;
    [SerializeField] private PageUI _interactivePage;
    [SerializeField] private GameObject _fileBrowser;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _settingsButton.onClick.AddListener(() =>
        {
            Hide();
            _settingsMenuController.Show();
        });

        _quitButton.onClick.AddListener(() =>
        {
            Hide();
            _quitMenuController.Show();
        });

        _adminButton.onClick.AddListener(() =>
        {
            Hide();
            _adminPage.Show();
        });

        _interactiveButton.onClick.AddListener(() =>
        {
            Hide();
            _interactivePage.Show();
        });
    }

    private void Start()
    {
        _fileBrowser.SetActive(false);
        // GetComponentInParent<GameObject>().SetActive(false);
        transform.parent.gameObject.SetActive(false);
    }

    #endregion
}
