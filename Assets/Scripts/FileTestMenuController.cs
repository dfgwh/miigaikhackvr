﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using SimpleFileBrowser;
using System.Collections;
using UnityEngine.Video;
using UnityEngine.Events;


public class FileTestMenuController : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    private void Start()
    {
        FileBrowser.AddQuickLink("Users", "C:\\Users", null);
        FileBrowser.SetDefaultFilter(".jpg");

        FileBrowser.PathSet += SetPath;
    }

    public UnityAction<string> setPath;
    
    private void SetPath(string path)
    {
        Debug.Log(path);
        videoPlayer.Stop();
        videoPlayer.time = 0;
        videoPlayer.url = path;
        videoPlayer.Play();
    }
}
