﻿using SimpleFileBrowser;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class AdminMainController : PageUI
{

    #region Unity Editor

    [SerializeField] private Text _url;
    [SerializeField] private VideoPlayer _videoPlayer;

    [SerializeField] private Button _playButton;
    [SerializeField] private Button _pauseButton;
    [SerializeField] private Slider _valueSlider;
    [SerializeField] private Button _newURLButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private PageUI _backPage;

    [SerializeField] private GameObject _fileBrowser;

    #endregion

    #region Private Fields

    private string _currentPath = "";

    #endregion


    #region Unity Methods

    private void Awake()
    {
        _playButton.onClick.AddListener(() =>
        {
            if (_currentPath != "")
            {
                _videoPlayer.Play();
            }
        });

        _pauseButton.onClick.AddListener(() =>
        {
            if (_currentPath != "")
            {
                _videoPlayer.Pause();
            }
        });

        _newURLButton.onClick.AddListener(() =>
        {
            FileBrowser.AddQuickLink("Users", "C:\\Users", null);
            _fileBrowser.SetActive(true);
            transform.parent.gameObject.SetActive(false);
            Hide();
        });

        _backButton.onClick.AddListener(() =>
        {
            _backPage.Show();

            Hide();
        });

        _valueSlider.onValueChanged.AddListener((value) =>
        {
            if (_currentPath != "")
            {
                _videoPlayer.time = _videoPlayer.url.Length * value;
            }
        });
    }

    private void Start()
    {
        FileBrowser.PathSet += SetPath;
        Show();
    }

    private void Update()
    {
        if (_videoPlayer.isPlaying)
        {
            _valueSlider.value = (float)(_videoPlayer.time / _videoPlayer.url.Length);
        }
    }

    #endregion

    #region Private Methods

    private void SetPath(string path)
    {
        Debug.Log(path);
        _videoPlayer.Stop();
        _videoPlayer.time = 0;
        _currentPath = path;
        _videoPlayer.url = path;
        _url.text = path;
        transform.parent.gameObject.SetActive(true);
        Show();
    }

    #endregion

}
