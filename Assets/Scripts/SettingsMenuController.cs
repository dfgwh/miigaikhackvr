﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuController : PageUI
{
    #region Unity Editor

    [Header("UI")]
    [SerializeField] private Slider _audioVolume;
    [SerializeField] private Button _backButton;

    [Header("Pages")]
    [SerializeField] private MainMenuController _mainMenuController;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _audioVolume.onValueChanged.AddListener((value) =>
        {
            Debug.Log("new volume value: " + value);
        });
        _backButton.onClick.AddListener(() =>
        {
            Hide();
            _mainMenuController.Show();
        });
    }

    #endregion
}
