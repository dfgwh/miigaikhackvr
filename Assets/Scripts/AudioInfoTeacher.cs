﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioInfoTeacher : MonoBehaviour
{

    #region Properties

    public static AudioInfoTeacher Instance
    {
        get => instance;
        set => instance = value;
    }

    #endregion


    #region Private Fields

    private static AudioInfoTeacher instance = null;
    private AudioClip _currentAudioClip;
    private AudioSource _audioSource;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    #endregion

    #region Public Methods

    public void SetAudioClip(AudioClip clip)
    {
        _currentAudioClip = clip;
        _audioSource.clip = _currentAudioClip;
    }

    public void StopAudio()
    {
        if (_audioSource.clip)
        {
            _audioSource.Stop();
            _audioSource.time = 0;
        }
        else
        {
            Debug.Log("AudioInfoTeacher: absent clip");
        }
    }

    public void StartAudio()
    {
        if (_audioSource.clip)
        {
            _audioSource.Play();
        }
        else
        {
            Debug.Log("AudioInfoTeacher: absent clip");
        }
    }
    

    #endregion
}
