﻿using UnityEngine;

public class UserNaming : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private TextMesh _usernameText;

    #endregion

    #region Unity Methods

    private void Start()
    {
        _usernameText.text = "User " + Random.Range(0, 1000).ToString();
    }

    #endregion
}
