﻿using UnityEngine;
using UnityEngine.UI;

public class PosterController : MonoBehaviour
{
    #region Unity Editor

    [Header("UI")]
    [SerializeField] private Button _launchAudioListenerButton;
    [SerializeField] private Button _stopAudioListenerButton;

    [Header("Audio")]
    [SerializeField] private AudioClip _audioClip;

    #endregion

    #region Unity Editor

    private void Awake()
    {
        _launchAudioListenerButton.onClick.AddListener(() =>
        {
            if (!_audioClip)
            {
                Debug.Log("<color=red>Poster: absent clip for AudioInfoTeacher</color>");
            }
            {
                AudioInfoTeacher.Instance.StopAudio();
                AudioInfoTeacher.Instance.SetAudioClip(_audioClip);
                AudioInfoTeacher.Instance.StartAudio();
            }
        });

        _stopAudioListenerButton.onClick.AddListener(() =>
        {
            AudioInfoTeacher.Instance.StopAudio();
        });
    }

    #endregion
}
